IntelligentTriggerTest
======================

This is a demo app for the intelligent triggering library (InterruptMe).
The app periodically senses a mobile phones location, acceleration and time,
and queries the user to find out if the moment was suitable for interruption.
After twenty training points, a classifier of interruptible moments is trained,
and from there on the notifications arrive according to the classified context.

Comments in the code should provide sufficient instructions for embedding the
library into other apps. The library can be references through jcentral, just add
the following line to your gradle file:

compile 'org.ubhave.intelligenttrigger:interruptme:1.0'

The app's souce code is at: https://bitbucket.org/veljkop/intelligenttriggertest
The InterruptMe library is at: https://bitbucket.org/veljkop/intelligenttrigger
The associated machine learning library for Android is at:
https://github.com/vpejovic/MachineLearningToolkit/

Copyright (c) 2013, University of Birmingham, UK
Copyright (c) 2015, University of Ljubljana, Slovenia

Veljko Pejovic,  <Veljko.Pejovic@fri.uni-lj.si>


This code was developed as part of the EPSRC Ubhave (Ubiquitous and Social
Computing for Positive Behaviour Change) Project. For more information, please visit
http://www.ubhave.org

Permission to use, copy, modify, and/or distribute this software for any purpose with
or without fee is hereby granted, provided that the above copyright notice and this
permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.