package si.uni_lj.fri.lrss.intelligenttriggertest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity{

    private static final String TAG = "MainActivity";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        ApplicationContext.getContext().startService(new Intent(ApplicationContext.getContext(), ITService.class));
        KeepAliveAlarm.startAlarm(ApplicationContext.getContext());
        KeepAliveAlarm.checkTrigger(ApplicationContext.getContext());
    }
}
