package si.uni_lj.fri.lrss.intelligenttriggertest.utils;

public class Constants {

    public static final long WAKE_UP_PERIOD = 30*60*1000;

    public static final long ACCELEROMETER_SENSING_WINDOW_LONG = 30*1000;
    public static final long ACCELEROMETER_SLEEP_INTERVAL = 2*60*1000;
    public static final long LOCATION_SLEEP_INTERVAL = 2*60*1000;

    // This is for Tint = 60*12, Tsample = 21 and Nexp = 3;
    public static final double RANDOM_TRIGGER_P = 0.08823529411;

    public static final int MIN_INTERRUPTIBILITY_POINTS = 20;

    public static final String SERVICE_INSTRUCTION = "service_instruction";
    public static final int CHECK_TRIGGERS = 1;
    public static final int TRAIN_INTER_LEARNER = 2;
    public static final String TRAIN_WITH_VALUE = "train_with_value";

    public static final String CURRENT_TRIGGER_NO = "triggerNumber";

    public static final String CURRENT_INTER_TRIGGER_NO = "interTriggerNumber";

    public static final String TRIGGER_TIME = "interTriggerTime";
    public static final String LAST_TRIGGER_TIME = "lastTriggerTime";
    public static final String LAST_TRIGGER_TYPE = "lastTriggerType";
    public static final String TRAINED_INTER_TRIGGER_INSTANCE = "trainedInterTriggerInstance";

    //Max two days without a notification on the interruptibility trigger
    public static final long MAX_NO_TRIGGER_TIME = 2*24*60*60*1000;
}
