package si.uni_lj.fri.lrss.intelligenttriggertest;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import org.ubhave.intelligenttrigger.learners.InterruptibilityLearner;

import si.uni_lj.fri.lrss.intelligenttriggertest.utils.Constants;

/**
 * Created by veljko on 22/12/15.
 */

public class SurveyActivity extends AppCompatActivity {

    private static final String TAG = "SurveyActivity";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");

        super.onCreate(savedInstanceState);

        setContentView(R.layout.survey_activity);
        NotificationManager nm = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.cancel(1);
    }

    @Override
    protected void onNewIntent(Intent intent)
    {
        Log.d(TAG, "onNewIntent");
        super.onNewIntent(intent);

        NotificationManager nm = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.cancel(1);
    }

    public void trainClassifier(View v){

        RadioGroup rGroup = (RadioGroup)findViewById(R.id.interruptibility_group);
        int checkedSelection = rGroup.getCheckedRadioButtonId();
        if (checkedSelection != -1){
            RadioButton checkedRadioButton = (RadioButton)rGroup.findViewById(checkedSelection);

            String value = "";

            switch(checkedRadioButton.getId()) {
                case R.id.interruptibility_yes_radio:
                    value = InterruptibilityLearner.INTERRUPTIBLE_YES;
                    break;
                case R.id.interruptibility_no_radio:
                    value = InterruptibilityLearner.INTERRUPTIBLE_NO;
                    break;
            }

            if (value.length() > 0) {
                Intent serviceIntent = new Intent(ApplicationContext.getContext(), ITService.class);
                serviceIntent.putExtra(Constants.SERVICE_INSTRUCTION, Constants.TRAIN_INTER_LEARNER);
                serviceIntent.putExtra(Constants.TRAIN_WITH_VALUE, value);
                ApplicationContext.getContext().startService(serviceIntent);
            }
        }

        finish();
    }
}
