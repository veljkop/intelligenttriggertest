package si.uni_lj.fri.lrss.intelligenttriggertest;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;

import si.uni_lj.fri.lrss.intelligenttriggertest.utils.Constants;

/**
 * Android can destroy our app, triggers included. Thus, we periodically check if the triggers
 * are still alive.
 */
public class KeepAliveAlarm extends BroadcastReceiver
{

    private static final String TAG = "KeepAliveAlarm";

    private static final Object lock = new Object();

    public void onReceive(Context context, Intent intent)
    {
        Log.d(TAG, "onReceive");

        String action = intent.getAction();

        if (action != null) {

            checkTrigger(context);

            if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
                startAlarm(context);
            }
       }
    }

    public static void startAlarm(Context context)
    {
        synchronized (lock)
        {

            Log.d(TAG, "startAlarm");

            AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent("si.uni_lj.fri.lrss.intelligenttriggertest.KEEP_ALIVE");
            PendingIntent pi = PendingIntent.getBroadcast(context, 1, intent, 0);

            int type = AlarmManager.ELAPSED_REALTIME_WAKEUP;
            long interval = Constants.WAKE_UP_PERIOD;
            long triggerTime = SystemClock.elapsedRealtime() + interval;

            am.setRepeating(type, triggerTime, interval, pi);
        }
    }

    public static void checkTrigger(Context context)
    {
        Intent serviceIntent = new Intent(ApplicationContext.getContext(),ITService.class);
        serviceIntent.putExtra(Constants.SERVICE_INSTRUCTION, Constants.CHECK_TRIGGERS);
        ApplicationContext.getContext().startService(serviceIntent);
    }
}