package si.uni_lj.fri.lrss.intelligenttriggertest;

import android.app.Application;
import android.content.Context;

public class ApplicationContext extends Application
{
    private static ApplicationContext instance;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public ApplicationContext()
    {
        instance = this;
    }

    public static Context getContext()
    {
        return instance;
    }
}