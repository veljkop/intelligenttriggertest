package si.uni_lj.fri.lrss.intelligenttriggertest;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.ubhave.sensormanager.ESException;
import com.ubhave.sensormanager.ESSensorManager;
import com.ubhave.sensormanager.config.GlobalConfig;
import com.ubhave.sensormanager.config.sensors.pull.LocationConfig;
import com.ubhave.sensormanager.config.sensors.pull.PullSensorConfig;
import com.ubhave.sensormanager.sensors.SensorUtils;

import org.ubhave.intelligenttrigger.IntelligentTriggerManager;
import org.ubhave.intelligenttrigger.IntelligentTriggerReceiver;
import org.ubhave.intelligenttrigger.learners.InterruptibilityLearner;
import org.ubhave.intelligenttrigger.learners.Learner;
import org.ubhave.intelligenttrigger.learners.LearnerResultBundle;
import org.ubhave.intelligenttrigger.triggers.config.BasicTriggerConfig;
import org.ubhave.intelligenttrigger.triggers.definitions.SensorTriggerDefinition;
import org.ubhave.intelligenttrigger.utils.ITException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import si.uni_lj.fri.lrss.intelligenttriggertest.utils.Constants;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Instance;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.MLException;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Value;

/**
 * Interaction with the intelligent triggering library is handled via a Service (here ITService).
 * Terminology first:
 * - Trigger: defines the conditions under which the library should notify the receiver
 * - Learner: a wrapper for a classifier that infers high level features
 * - IntelligentTriggerReceiver: a class that is notified when a trigger condition is satisfied
 * The service instantiates trigger(s) and registers them with the library. The library notifies the
 * service when the trigger condition is satisfied. The service trains the interruptibility learner
 * with the feedback info (e.g. if the user was indeed interruptible).
 */
public class ITService extends Service implements IntelligentTriggerReceiver {

    private static final String TAG = "ITService";

    private static ITService mITservice;
    private IntelligentTriggerManager mITmanager;
    private static final Object mLock = new Object();

    public void saveState() {
        Log.d(TAG, "saveState");
        mITmanager.saveToPersistent();
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate");

        if (mITservice == null) {

            synchronized (mLock) {

                mITservice = this;

                try {

                    // Sensing parameters (how often to sense the context, location granularity, etc),
                    // are configured through the sensing library.
                    ESSensorManager sensorManager = ESSensorManager.getSensorManager(ApplicationContext.getContext());
                    sensorManager.setGlobalConfig(GlobalConfig.ACQUIRE_WAKE_LOCK, true);
                    sensorManager.setSensorConfig(
                            SensorUtils.SENSOR_TYPE_ACCELEROMETER,
                            PullSensorConfig.SENSE_WINDOW_LENGTH_MILLIS,
                            Constants.ACCELEROMETER_SENSING_WINDOW_LONG);
                    sensorManager
                            .setSensorConfig(
                                    SensorUtils.SENSOR_TYPE_ACCELEROMETER,
                                    PullSensorConfig.POST_SENSE_SLEEP_LENGTH_MILLIS,
                                    Constants.ACCELEROMETER_SLEEP_INTERVAL);
                    sensorManager.setSensorConfig(SensorUtils.SENSOR_TYPE_LOCATION,
                            PullSensorConfig.POST_SENSE_SLEEP_LENGTH_MILLIS,
                            Constants.LOCATION_SLEEP_INTERVAL);
                    sensorManager.setSensorConfig(SensorUtils.SENSOR_TYPE_LOCATION,
                            LocationConfig.ACCURACY_TYPE,
                            LocationConfig.LOCATION_ACCURACY_FINE);
                } catch (ESException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        }
    }

    /**
     * Android may destroy the app/service. Thus, we need to check if the triggers are instantiated
     * every once a while.
     * @throws ITException
     */
    public void checkTriggers() throws ITException {

        mITmanager = IntelligentTriggerManager.getTriggerManager(ApplicationContext.getContext());

        SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(ApplicationContext.getContext());

        // The last time the trigger was fired
        long lastTriggerTime = settings.getLong(Constants.LAST_TRIGGER_TIME, -1);
        long currentTime = System.currentTimeMillis();

        // Intelligent trigger has different learners (full list in org.ubhave.intelligenttrigger.utils.Constants)
        // Different learners support different triggers - you are probably most interested in the
        // trigger that fires when a user is "interruptible". However, you can also have triggers
        // that fire when a user is at home/work, or at a particular time.
        // Here, we need a reference to the learner, as we want to check the state of the learner
        // when instantiating a trigger (see below).
        Learner interLearner = mITmanager.getLearner(org.ubhave.intelligenttrigger.utils.Constants.MOD_INTERRUPTIBILITY);

        // If the trigger has not been fired for more than the MAX_NO_TRIGGER_TIME, we switch to
        // random triggering, which fires the trigger with a certain probability after every
        // sensing cycle.
        if ((lastTriggerTime > -1) &&
                (currentTime - lastTriggerTime > Constants.MAX_NO_TRIGGER_TIME)) {
            interLearner.randomTriggerSwitch(true, Constants.RANDOM_TRIGGER_P);
        }
        // If the learner has not seen enough data points to be considered reliable, we use
        // random triggering, which fires the trigger with a certain probability after every
        // sensing cycle. Otherwise, if the learner has been trained with data, we turn the random
        // firing off.
        else {
            if (interLearner.instanceQueueSize() < Constants.MIN_INTERRUPTIBILITY_POINTS){
                interLearner.randomTriggerSwitch(true, Constants.RANDOM_TRIGGER_P);
            } else {
                if (interLearner.isTrained()) {
                    interLearner.randomTriggerSwitch(false, Constants.RANDOM_TRIGGER_P);
                }
            }
        }

        // Triggers are labelled by a unique name. They are defined by trigger configuration.
        // Sensor-based triggers require sensing. For such triggers we define the condition that
        // needs to be met in order for the trigger to be fired. Most probably, you are interested
        // in condition "user is interruptible" (shown here):
        if (!mITmanager.triggerExists("when_interruptible")) {

            BasicTriggerConfig config = new BasicTriggerConfig(true);
            config.addParam(org.ubhave.intelligenttrigger.utils.Constants.NUMBER_OF_NOTIFICATIONS, 3);
            config.addParam(org.ubhave.intelligenttrigger.utils.Constants.MAX_DAILY_NOTIFICATION_CAP, 3);
            config.addParam(org.ubhave.intelligenttrigger.utils.Constants.MIN_TRIGGER_INTERVAL_MINUTES,20);

            SensorTriggerDefinition def = new SensorTriggerDefinition(config);
            def.addCondition(org.ubhave.intelligenttrigger.utils.Constants.MOD_INTERRUPTIBILITY,
                    SensorTriggerDefinition.REL_EQUALS,
                    InterruptibilityLearner.INTERRUPTIBLE_YES);

            // For location
            //def.addCondition(org.ubhave.intelligenttrigger.utils.Constants.MOD_LOCATION, SensorTriggerDefinition.REL_EQUALS, LocationLearner.LOCATION_HOME);

            // For time
            ///config.addParam(org.ubhave.intelligenttrigger.utils.Constants.DO_NOT_DISTURB_AFTER_MINUTES, 855);
            //config.addParam(org.ubhave.intelligenttrigger.utils.Constants.DO_NOT_DISTURB_BEFORE_MINUTES, 800);
            //TimeTriggerDefinition defTime = new TimeTriggerDefinition(10002, config);

            Log.d(TAG, "adding trigger");
            mITmanager.addIntelligentTrigger("when_interruptible", def, this);
        }
    }


    public void saveTriggers() {
        try {
            mITmanager = IntelligentTriggerManager.getTriggerManager(ApplicationContext.getContext());
            mITmanager.saveToPersistent();
        } catch (ITException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent != null) {
            int command = intent.getIntExtra(Constants.SERVICE_INSTRUCTION, 0);

            switch (command) {
                case Constants.CHECK_TRIGGERS:
                    try {
                        checkTriggers();
                    } catch (ITException e) {
                        e.printStackTrace();
                    }
                    break;
                case Constants.TRAIN_INTER_LEARNER:
                    String value = intent.getStringExtra(Constants.TRAIN_WITH_VALUE);
                    SharedPreferences settings = PreferenceManager
                            .getDefaultSharedPreferences(ApplicationContext.getContext());
                    int currentInterTriggerInstanceNo = settings
                            .getInt(Constants.CURRENT_INTER_TRIGGER_NO, 0);
                    trainLearnerFromFeedback(value, currentInterTriggerInstanceNo);
                    break;
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onTriggerReceived(String triggerID,ArrayList<LearnerResultBundle> bundles) {

        SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(ApplicationContext.getContext());
        SharedPreferences.Editor editor = settings.edit();
        int currentTriggerNo = settings.getInt(Constants.CURRENT_TRIGGER_NO, 0);
        editor.putInt(Constants.CURRENT_TRIGGER_NO, ++currentTriggerNo);
        editor.apply();

        boolean trained;
        boolean random;

        // If the previous notification fired by the triggering was fired due to our interruptiblity
        // trigger, and if the user has not answered the notification, we assume that the moment
        // was not suitable for interruption. Thus, we train the interruptibility learner with the
        // sensed data and the label "not interruptible".
        int lastTriggerType = settings.getInt(Constants.LAST_TRIGGER_TYPE, -1);
        if (lastTriggerType ==
                org.ubhave.intelligenttrigger.utils.Constants.MOD_INTERRUPTIBILITY) {
            int currentInterTriggerInstanceNo = settings.getInt(Constants.CURRENT_INTER_TRIGGER_NO, 0);
            boolean trainedInterTriggerInstance = settings.getBoolean(
                    Constants.TRAINED_INTER_TRIGGER_INSTANCE, false);
            if ((!trainedInterTriggerInstance) && (currentInterTriggerInstanceNo>0)) {
                Log.d(TAG, "Train InterruptibilityLearner on unanswered instance");
                trainLearnerFromFeedback(InterruptibilityLearner.INTERRUPTIBLE_NO,
                        currentInterTriggerInstanceNo);
            }
        }

        int currentTriggerType = -1;
        int currentNotifTriggerNo = -1;
        if (bundles != null) {
            LearnerResultBundle firstInstance = bundles.get(0);
            currentTriggerType = firstInstance.getLearnerID();
            currentNotifTriggerNo = firstInstance.getInstanceID();
            trained = firstInstance.getIsTrained();
            random = firstInstance.getIsRandom();

            Log.d(TAG, "Received instance from triggerID: " + triggerID+
                    " instance number: "+currentNotifTriggerNo+
                    " trigger trained: "+trained+
                    " from random triggering: "+random);

            // We perform the check, whether the learner is trained with a sufficient amount of data
            // after every notification.
            if (currentTriggerType ==
                    org.ubhave.intelligenttrigger.utils.Constants.MOD_INTERRUPTIBILITY) {
                editor.putInt(Constants.CURRENT_INTER_TRIGGER_NO, currentNotifTriggerNo);
                editor.putBoolean(Constants.TRAINED_INTER_TRIGGER_INSTANCE, false);
                editor.apply();
                try {
                    mITmanager = IntelligentTriggerManager.getTriggerManager(ApplicationContext.getContext());
                    Learner interLearner = mITmanager
                            .getLearner(org.ubhave.intelligenttrigger.utils.Constants.MOD_INTERRUPTIBILITY);
                    if (interLearner.instanceQueueSize() < Constants.MIN_INTERRUPTIBILITY_POINTS){
                        interLearner.randomTriggerSwitch(true, Constants.RANDOM_TRIGGER_P);
                    } else {
                        if (interLearner.isTrained()) {
                            interLearner.randomTriggerSwitch(false, Constants.RANDOM_TRIGGER_P);
                        }
                    }
                } catch (ITException e) {
                    e.printStackTrace();
                }
            }

            // Keep info on the last seen notification, so that we train the learner on it.
            editor.putInt(Constants.LAST_TRIGGER_TYPE, currentTriggerType);
            editor.putLong(Constants.LAST_TRIGGER_TIME, System.currentTimeMillis());
            editor.apply();
        }

        // Send notification out with the desired activity. In this example, we have an activity
        // SurveyActivity that explicitly asks the user whether it was a good moment to interrupt.
        Context context = ApplicationContext.getContext();
        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        Intent surveyIntent = new Intent(context, SurveyActivity.class);
        surveyIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        Notification notification;
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, surveyIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);

        notification = new NotificationCompat.Builder(context)
                .setContentTitle("InterruptMe")
                .setContentText("Rate this notification moment")
                .setSmallIcon(android.R.drawable.ic_dialog_alert)
                .setWhen(System.currentTimeMillis())
                .setContentIntent(contentIntent)
                .build();
        notification.defaults |= Notification.DEFAULT_ALL;
        notificationManager.notify(1, notification);
    }

    // NOTE: not used in the interruptibility learner example
    // Trains a location learner from a text file containing lat, lng, home/work label info.
    public void trainLearnerFromFile(){
        Log.d(TAG, "trainLearnerFromFile");
        InputStream inputStream = ApplicationContext.getContext()
                .getResources().openRawResource(R.raw.locations_trim);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        try {
            String line = reader.readLine();
            ArrayList<Instance> instances = new ArrayList<Instance>();
            while ( (line = reader.readLine()) != null) {
                Instance i = new Instance();
                String[] tokens = line.split(",");
                Value lat = new Value(Double.parseDouble(tokens[0]), Value.NUMERIC_VALUE);
                Value lon = new Value(Double.parseDouble(tokens[1]), Value.NUMERIC_VALUE);
                Value label = new Value(tokens[2], Value.NOMINAL_VALUE);
                i.addValue(lat);
                i.addValue(lon);
                i.addValue(label);
                instances.add(i);
            }
            mITmanager = IntelligentTriggerManager.getTriggerManager(ApplicationContext.getContext());
            mITmanager.trainLearner(org.ubhave.intelligenttrigger.utils.Constants.MOD_LOCATION, instances);
        } catch (IOException | ITException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void trainLearnerFromFeedback(Object value, int instanceNo){
        try {
            Log.d(TAG, "Train interruptibility learner with instance "+instanceNo+" and value "+value);

            mITmanager = IntelligentTriggerManager.getTriggerManager(ApplicationContext.getContext());

            mITmanager.trainLearnerFromFeedback(
                    org.ubhave.intelligenttrigger.utils.Constants.MOD_INTERRUPTIBILITY,
                    instanceNo,
                    value);

            SharedPreferences settings = PreferenceManager
                    .getDefaultSharedPreferences(ApplicationContext.getContext());
            SharedPreferences.Editor editor = settings.edit();
            editor.putBoolean(Constants.TRAINED_INTER_TRIGGER_INSTANCE, true);
            editor.apply();
        } catch (ITException | MLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        saveState();
        super.onDestroy();
    }
}